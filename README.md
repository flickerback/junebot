# Junebot

[![GitLab Repository](https://img.shields.io/badge/repository-GitLab-brightgreen)](https://gitlab.com/flickerback/junebot)

Junebot is a Discord.js bot built with NodeJS.

## Git Structure

```
junebot
 ├── node_modules
 ├── src
 ├── package.json
 └── README.md
```

## Tools Used

- [Node.js](https://nodejs.org/)

## Core Modules

- [discord.js](https://discord.js.org/)
- [dotenv](https://www.npmjs.com/package/dotenv)
- [mongoose](https://mongoosejs.com/)

## How to Run

Make sure you have Node.js installed.

```bash
git clone https://gitlab.com/flickerback/junebot.git
cd junebot
npm install
node .
```

## Additional Information

- For environment variables, create a `.env` file in the project directory and add your Discord bot token:

```
TOKEN=your_discord_bot_token_here
```

- Customize the bot's functionality by modifying the files within the `src` directory.

- For any issues or feature requests, feel free to open an [issue](https://gitlab.com/flickerback/junebot/-/issues).