module.exports = async (client, reaction, user) => {
    if (reaction.partial) {
        try {
            await reaction.fetch();
        } catch (error) {
            console.error('Something went wrong when fetching the message: ', error);
            return;
        }
    }

    console.log(`${reaction.message.author.username}'s message "${reaction.message.content}" gained a reaction!`);
    console.log(`${user.tag} reacted with "${reaction.emoji.name}".`);
};
