const {devs, testServer} = require('../../../config.json');
const getLocalCommands = require('../../utility/getLocalCommands');

module.exports = async (client, interaction)=>{
    if (!interaction.isChatInputCommand()){
        return;
    }
    const localComamnds=getLocalCommands();

    try {
        const commandObject = localComamnds.find(cmd=>cmd.name===interaction.commandName);
        if(!commandObject){
            return;
        }
        if(commandObject.devOnly){
            if(!devs.includes(interaction.member.id)){
                interaction.reply({
                    content: 'Only for epic cool application devs.',
                    ephemeral: true,
                });
            return;
            }
        }

        if(commandObject.testOnly){
            if(!(interaction.guild.id===testServer)){
                interaction.reply({
                    content: 'This command cannot be ran here.',
                    ephemeral: true,
                });
            return;
            }
        }

        await commandObject.callback(client, interaction);
        
    } catch (error) {
        console.log(`There was an error running this command: ${error}`);
    }

};
