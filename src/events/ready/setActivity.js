const { Client, GatewayIntentBits, ActivityType } = require ('discord.js');
module.exports = (client) => {
    client.user.setPresence({
        activities: [{ name: `June's Journey`, type: ActivityType.Playing }],
        status: 'online',
      });
  };