const { testServer } = require('../../../config.json');
const getLocalCommands = require('../../utility/getLocalCommands');

module.exports = async (client) => {
  try {
    const localCommands = getLocalCommands();

    for (const localCommand of localCommands) {
      const { name, description, options } = localCommand;

      const applicationCommands = await client.guilds.cache
        .get(testServer)
        .commands.fetch();

      const existingCommand = applicationCommands.find(
        (cmd) => cmd.name === name
      );

      if (existingCommand) {
        if (localCommand.deleted) {
          await existingCommand.delete();
          console.log(`🗑 Deleted command "${name}".`);
          continue;
        }

        const commandData = {
          name,
          description,
          options,
        };

        const commandChanges = {};

        // Check for differences and update the commandData
        if (existingCommand.description !== description) {
          commandChanges.description = description;
        }

        // Compare options array using a deep comparison
        if (
          JSON.stringify(existingCommand.options) !==
          JSON.stringify(options)
        ) {
          commandChanges.options = options;
        }

        if (Object.keys(commandChanges).length > 0) {
          await existingCommand.edit(commandChanges);
          console.log(`🔁 Edited command "${name}".`);
        }
      } else {
        if (localCommand.deleted) {
          console.log(
            `⏩ Skipping registering command "${name}" as it's set to delete.`
          );
          continue;
        }

        await client.guilds.cache.get(testServer).commands.create({
          name,
          description,
          options,
        });

        console.log(`👍 Registered command "${name}".`);
      }
    }
  } catch (error) {
    console.log(`There was an error: ${error}`);
  }
};
