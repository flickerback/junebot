const {
    ApplicationCommandOptionType,
    PermissionFlagsBits,
  } = require('discord.js');
const parseDiceExpression = require('../../utility/parseDiceExpression');
const rollCustomDice = require('../../utility/rollCustomDice');


  module.exports = {
    deleted: false,
    name: 'roll',
    description: 'Roll dice using the format [number of dice]d[number of sides] + optional modifier. Example: 1d20+5',
    // devOnly: Boolean,
    // testOnly: Boolean,
    options: [
      {
        name: 'expression',
        description: 'e.g. 1d20+5',
        required: true,
        type: ApplicationCommandOptionType.String,
      },
    ],
    //permissionsRequired: [PermissionFlagsBits.Administrator],
    //botPermissions: [PermissionFlagsBits.Administrator],

// TODO: print who requested the roll, what the parameters were, and what each individual dice roll was, along with the final result

    callback: (client, interaction) => {
      const userInput = parseDiceExpression(interaction.options.get('expression').value);
      const rollResult = rollCustomDice(userInput.numberOfDice,userInput.numberOfSides,userInput.modifier);
      rollsDelimited = rollResult.rolls.join(', ');
      //console.log()
      //console.log(userInput);
      //console.log(rollResult.rolls);
      interaction.reply('Request: `'+ interaction.options.get('expression').value + '` Roll: `[' +rollsDelimited+'] ('+ rollResult.total + ')` Result: `'+ rollResult.finalTotal+"`");
    },
  };