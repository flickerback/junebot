module.exports = (numberOfDice, sides, modifier) => {
  // Validate input
  if (
      typeof numberOfDice !== 'number' ||
      numberOfDice <= 0 ||
      !Number.isInteger(numberOfDice) ||
      typeof sides !== 'number' ||
      sides <= 0 ||
      !Number.isInteger(sides) ||
      typeof modifier !== 'number' ||
      !Number.isInteger(modifier)
    ) {
      throw new Error('Invalid input. Please provide valid numbers.');
    }
  
    let total = 0;
    let finalTotal = 0;
    let rolls =[];
  
    // Roll the specified number of dice
    for (let i = 0; i < numberOfDice; i++) {
      // Generate a random number between 1 and the number of sides for each roll
      let roll=Math.floor(Math.random() * sides) + 1;
      rolls.push(roll);
      total += roll;
    }
    
    // Add the modifier to the total
    finalTotal = total+modifier;
  
    return {
      total,
      finalTotal,
      rolls,
    };
  };