  module.exports = (expression) => {
    // Regular expression to match the format "1d20+6"
    const regex = /^(\d+)d(\d+)([+-]\d+)?$/;
  
    // Use the regex to extract components
    const match = expression.match(regex);
  
    if (!match) {
      throw new Error('Invalid dice expression format. Please use the format "1d20+6".');
    }
  
    // Extract the matched components
    const numberOfDice = parseInt(match[1], 10);
    const numberOfSides = parseInt(match[2], 10);
    const modifier = match[3] ? parseInt(match[3], 10) : 0;
  
    return {
      numberOfDice,
      numberOfSides,
      modifier,
    };
  }